#pragma once
#include "../libgraphqlparser/AstVisitor.h"
#include <iterator>

namespace grp_inj{
    namespace fga = facebook::graphql::ast;

    class QueryVisitor: public fga::visitor::AstVisitor {
        public:
            struct Function {
                std::string name;
                std::vector<std::string> args;
            };
            using Functions = std::vector<Function>;
        public:
            inline const Functions getResult() {
                return result;
            }
        private:
            bool visitName(const fga::Name &name) override;
            bool visitSelectionSet(const fga::SelectionSet &selectionSet) override;
            void endVisitSelectionSet(const fga::SelectionSet &selectionSet) override;
            bool visitArgument(const fga::Argument &argument) override;
            bool visitStringValue(const fga::StringValue &stringValue) override;
        private:
            Functions result;
            bool add = false;
    }; 
}