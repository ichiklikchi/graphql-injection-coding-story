#include <iostream>
#include <sstream>
#include <fstream>
#include "AuthenticationApp.h"

namespace grp_inj {
    
    AuthenticationApp::AuthenticationApp() 
        : m_storage(createStorage())
        , m_query() {
        std::ifstream file("./assets/Authentication.graphql");
        std::stringstream buffer;
        buffer << file.rdbuf();
        m_query = buffer.str();
    }

    void AuthenticationApp::run() {
        std::string login, password;

        std::cout << "Enter login: ";
        std::cin >> login;

        if (not existsLogin(login)) {
            std::cout << "No login is found, " << login; 
        } else {

            std::cout << "Ok, " << login << ", enter your password: ";
            std::cin >> password;
            
            std::cout << "Password is" << (existsPassword(login, password)? "": "n't") << " correct!";
        }
        std::cout << "\nGoodbye!" << std::endl;
    }
    
    static void replace(std::string& str, std::string_view from, std::string_view to) {
        std::string::size_type begin = str.find(from); 
        if (begin != std::string::npos) {
            if (to.empty()) 
                str.erase(begin, begin + from.length());
            else 
                str.replace(begin, from.length(), to);
        }       
    }

    bool AuthenticationApp::existsLogin(std::string_view login) const {
        std::string query{m_query};
        replace(query, "$1", login);
        replace(query, "$2", "''");
        return m_storage->query(query);
    }
 
    bool AuthenticationApp::existsPassword(std::string_view login, std::string_view password) const {
        std::string query{m_query};
        replace(query, "$1", login);
        replace(query, "$2", password);
        return m_storage->query(query);
    }

} // namespace grp_inj
