#pragma once
#include "IStorage.h"
#include <map>

namespace grp_inj {

    class GrpStorage : public IStorage {
        public:
            GrpStorage();
        private:
            bool query(std::string_view query) const override;
            bool hasUser(
                std::string_view login, 
                std::string_view password) const;
        
    };
} // namespace grp_inj
