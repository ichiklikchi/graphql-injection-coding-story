#include "GrpStorage.h"
#include "QueryVisitor.h"
#include "../libgraphqlparser/GraphQLParser.h"
#include <iostream>
#include <cstdlib>
#include <sqlite3.h>
#include <memory>
#include <fstream>
#include <sstream>

namespace grp_inj {
    namespace {
        using DBPtr = std::unique_ptr<sqlite3, decltype(&sqlite3_close)>;
        DBPtr handler {nullptr, &sqlite3_close};
        
        std::string toString(const std::string& path) {
            std::ifstream file(path);
            std::stringstream buffer;
            buffer << file.rdbuf();
            return buffer.str();
        }    
    }
    
    GrpStorage::GrpStorage() {
        sqlite3* db = nullptr;
        const int result = sqlite3_open("./assets/users.db", &db);
        if (result == SQLITE_OK) {
            handler.reset(db);
            char* szErrMsg = nullptr;
            sqlite3_exec(db, toString("./assets/create.sql").data(), nullptr, 0, &szErrMsg);
            sqlite3_exec(db, toString("./assets/fill.sql").data(), nullptr, 0, &szErrMsg);
            if (szErrMsg)
                std::cerr << szErrMsg << std::endl;

        }
    }

    bool GrpStorage::query(std::string_view query) const {
        const char* error;
        auto node = facebook::graphql::parseString(query.data(), &error);
        if (not node ) {
            std::cout << error << std::endl;
            free((void*)(error));
        } else {
            QueryVisitor visitor;
            node->accept(&visitor);
            auto result = visitor.getResult();
            bool call = false;
            for (int index = 0; index < result.size(); ++index) {
                if ("hasUser" == result[index].name and result[index].args.size() == 2) {
                    auto& args = result[index].args;
                    return hasUser(args.front(), args.back());
                }
            }
        }
        return false;
    }

    bool GrpStorage::hasUser(std::string_view login, std::string_view password) const {
        bool result = false;
        if (handler) {
            sqlite3_stmt* stmt = nullptr;
            std::string select {"SELECT * FROM Users WHERE username = ?"};
            if (password != "''") {
                select += " and userpass = ?";
            }
            char* szErrMsg = nullptr;
            sqlite3_prepare_v2 (handler.get(), select.data(), select.length(), &stmt, NULL);
            sqlite3_bind_text(stmt, 1, login.data(), login.length(), SQLITE_STATIC);
            if (password != "''") {
                sqlite3_bind_text(stmt, 2, password.data(), password.length(), SQLITE_STATIC);
            }
            result = SQLITE_DONE != sqlite3_step(stmt);
            sqlite3_finalize(stmt);
            if(szErrMsg) std::cout << szErrMsg << std::endl;
        }
        return result;
    }

    StoragePtr createStorage() {
        return std::make_unique<GrpStorage>();
    }
} // namespace grp_inj
