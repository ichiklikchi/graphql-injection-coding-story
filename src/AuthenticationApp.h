#pragma once
#include "IStorage.h"

namespace grp_inj {

    class AuthenticationApp {
        public:
            AuthenticationApp();
            /*
             * \brief Run application 
             */
            void run();
        private:
            /*
             * \brief checks if login exists and take him from console
             */
            bool existsLogin(std::string_view login) const;
            /*
             * \brief checks if password exists and take him from console
             */
            bool existsPassword(std::string_view login, std::string_view password) const;
        private:
            StoragePtr m_storage;
            std::string m_query;
    };

} // namespace grp_inj
