#include "QueryVisitor.h"
#include <iterator>

namespace grp_inj {

    bool QueryVisitor::visitName(const fga::Name &name) {
        if (add) {
            result.push_back({name.getValue(), {} });
            add = false;
        }
        return add;
    }

    bool QueryVisitor::visitSelectionSet(const fga::SelectionSet &selectionSet) {
        add = true;
        return add;
    }

    void QueryVisitor::endVisitSelectionSet(const fga::SelectionSet &selectionSet) {
        add = false;
    }

    bool QueryVisitor::visitArgument(const fga::Argument &argument) {
        return not result.empty();
    }

    bool QueryVisitor::visitStringValue(const fga::StringValue &stringValue) {
        result.back().args.emplace_back(stringValue.getValue());
        return add;
    }
}